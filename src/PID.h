// #ifndef PID_H
// #define PID_H
// #include <vector>

// class PID {
// public:
//   /*
//   * Errors
//   */
//   double p_error;
//   double i_error;
//   double d_error;
//   double err;
//   double best_err;
//   double tol;
//   bool flag;
//   int first;
//   std::vector<double> p;
//   std::vector<double> dp;
//   int index;
//   bool restart;
//   int span;
//   double roll_avg;
//   double steer;

//   /*
//   * Coefficients
//   */ 
//   double Kp;
//   double Ki;
//   double Kd;

//   /*
//   * Constructor
//   */
//   PID();

//   /*
//   * Destructor.
//   */
//   virtual ~PID();

//   /*
//   * Initialize PID.
//   */
//   void Init(double Kp, double Ki, double Kd, bool flag);

//   /*
//   * Update the PID error variables given cross track error.
//   */
//   void UpdateError(double cte);

//   /*
//   * Calculate the total PID error.
//   */
//   double TotalError();
// };

// #endif /* PID_H */


#ifndef PID_H
#define PID_H

#include <vector>

class PID {
public:
  /*
  * Errors
  */
  double p_error;
  double i_error;
  double d_error;

  double err;
  double best_err;
  double tol;
  int restart;
  int horizon;
  bool flag;
  double p[3];
  double dp[3];
  int first;
  int index;
  /*
  * Coefficients
  */ 
  double Kp;
  double Ki;
  double Kd;

  /*
  * Constructor
  */
  PID();

  /*
  * Destructor.
  */
  virtual ~PID();

  /*
  * Initialize PID.
  */
  void Init(double Kp, double Ki, double Kd, bool tune_param);

  /*
  * Update the PID error variables given cross track error.
  */
  void UpdateError(double cte);

  /*
  * Calculate the total PID error.
  */
  double TotalError();
};

#endif /* PID_H */
